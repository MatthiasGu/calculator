package main.parser;

import main.model.expression.CompositeExpression;

import java.util.Arrays;

/**
 * Class to parse the user input into {@link CompositeExpression}s.
 */
public class ExpressionParser {

    private NumberParser numberParser;
    private OperatorParser operatorParser;

    public ExpressionParser() {
        numberParser = new NumberParser();
        operatorParser = new OperatorParser();
    }

    /**
     * Splits the input string on whitespace and initiates the recursive parsing method (see {@link #parseExpressions(String[])}.
     * @param inputString User input string (verbal representation of the expression).
     * @return {@link CompositeExpression} representing the expression input by the user.
     */
    public CompositeExpression splitAndParseExpressions(String inputString) {
        return parseExpressions(inputString.split("\\s"));
    }

    /**
     * Recursively parses the inputUnits into {@link CompositeExpression}s. A basic expression is 1 or 3 words long, i.e.
     * (number) or (number operator number). If the expression is longer than 3 words, take the first 3 words, creates
     * an {@link ArithmeticException} from those 3 words and then a {@link CompositeExpression} with the ArithmeticExpression
     * as the LHS and the remaining inputUnits as the RHS. This is done recursively until inputUnits are exhausted.
     * @param inputUnits List of words in the format (number operator number operator number).
     * @return {@link CompositeExpression} representing the expression.
     */
    public CompositeExpression parseExpressions(String[] inputUnits) {
        int expressionSize = inputUnits.length;
        try {
            // Single word, it can only be a number, as operator would be illegal.
            if (expressionSize == 1) {
                return new CompositeExpression(numberParser.parseNumber(inputUnits[0]));
            }
            // Three words, number operator number.
            else if (expressionSize == 3) {
                return new CompositeExpression(
                        numberParser.parseNumber(inputUnits[0]),
                        operatorParser.parseOperator(inputUnits[1]),
                        numberParser.parseNumber(inputUnits[2])
                );
            }
            // More than 3 words, has to be an odd number, otherwise the expression is illegal.
            else if (expressionSize > 3 && expressionSize % 2 != 0) {
                return new CompositeExpression(
                        new CompositeExpression(numberParser.parseNumber(inputUnits[0])),
                        operatorParser.parseOperator(inputUnits[1]),
                        parseExpressions(Arrays.copyOfRange(inputUnits, 2, inputUnits.length))
                );
            }
            // In case of illegal expression, return null.
            else {
                throw new Exception("You have entered an impossible expression! It should be of an odd length and have " +
                        "the format (number operator number) or just (number), repeated any number of times.");
            }

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }


}
