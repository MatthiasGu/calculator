package main.parser;

import main.exception.IllegalOperatorException;
import main.model.operator.Operator;

import java.util.HashMap;
import java.util.Map;

/**
 * Class to parse an {@link Operator} from a word.
 */
public class OperatorParser {

    /**
     * Stores the possible ways to represent the four operators.
     */
    private Map<String, Operator> operatorMap;

    public OperatorParser() {
        operatorMap = new HashMap<>();
        operatorMap.put("add", Operator.ADD);
        operatorMap.put("plus", Operator.ADD);
        operatorMap.put("subtract", Operator.SUBTRACT);
        operatorMap.put("minus", Operator.SUBTRACT);
        operatorMap.put("less", Operator.SUBTRACT);
        operatorMap.put("multiplied-by", Operator.MULTIPLY);
        operatorMap.put("times", Operator.MULTIPLY);
        operatorMap.put("divided-by", Operator.DIVIDE);
        operatorMap.put("over", Operator.DIVIDE);
    }

    /**
     * Returns the {@link Operator} representation of the given operator String (case insensitive).
     * @throws IllegalOperatorException If the operatorString is not accepted by the Calculator.
     */
    public Operator parseOperator(String operatorString) throws IllegalOperatorException{
        operatorString = operatorString.toLowerCase();
        if (operatorMap.containsKey(operatorString)) {
            return operatorMap.get(operatorString);
        } else {
            throw new IllegalOperatorException(operatorString);
        }
    }
}
