package main.parser;

import main.exception.IllegalNumberException;

import java.util.HashMap;
import java.util.Map;

/**
 * Class to parse a number from a word.
 */
public class NumberParser {

    /**
     * Stores the possible numeric values, zero to ten inclusive.
     */
    private Map<String, Double> numberMap;

    public NumberParser() {
        numberMap = new HashMap<>();
        numberMap.put("zero", 0.0);
        numberMap.put("one", 1.0);
        numberMap.put("two", 2.0);
        numberMap.put("three", 3.0);
        numberMap.put("four", 4.0);
        numberMap.put("five", 5.0);
        numberMap.put("six", 6.0);
        numberMap.put("seven", 7.0);
        numberMap.put("eight", 8.0);
        numberMap.put("nine", 9.0);
        numberMap.put("ten", 10.0);
    }

    /**
     * Returns the numeric representation of the given numberString (case insensitive).
     * @throws IllegalNumberException If the numberString is not accepted by the Calculator.
     */
    public double parseNumber(String numberString) throws IllegalNumberException {
        numberString = numberString.toLowerCase();
        if (numberMap.containsKey(numberString)) {
            return numberMap.get(numberString);
        } else {
            throw new IllegalNumberException(numberString);
        }
    }
}
