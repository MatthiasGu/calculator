package main.model.operator;

/**
 * Class to enumerate the arithmetic operators accepted by the {@link main.parser.OperatorParser}.
 */
public enum Operator {
    ADD(),
    SUBTRACT(),
    MULTIPLY(),
    DIVIDE()
}
