package main.model.expression;

import main.model.operator.Operator;

/**
 * Superclass for an arithmetic expression (number + / - * number)
 */
public abstract class ArithmeticExpression {

    private double leftOperand;
    private double rightOperand;

    public ArithmeticExpression(double leftOperand, double rightOperand) {
        this.leftOperand = leftOperand;
        this.rightOperand = rightOperand;
    }

    double getLeftOperand() {
        return leftOperand;
    }

    double getRightOperand() {
        return rightOperand;
    }

    /**
     * Evaluates the expression. Evaluation function is implemented differently depending on the type of Expression. See,
     * e.g. {@link AdditionExpression#evaluate()}
     * @return Result of performing the arithmetic operation.
     */
    public abstract double evaluate();

    /**
     * Factory for creating arithmetic expressions based on the {@link Operator}.
     * @return Representation of the arithmetic expression based on the operator, e.g. {@link AdditionExpression}.
     */
    public static ArithmeticExpression createExpressionFromOperator(double leftOperand, Operator operator, double rightOperand) {
        switch (operator) {
            case ADD:
                return new AdditionExpression(leftOperand, rightOperand);
            case SUBTRACT:
                return new SubtractionExpression(leftOperand, rightOperand);
            case MULTIPLY:
                return new MultiplicationExpression(leftOperand, rightOperand);
            case DIVIDE:
                return new DivisionExpression(leftOperand, rightOperand);
            default:
                return null;
        }
    }
}
