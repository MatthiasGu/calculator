package main.model.expression;

import main.model.operator.Operator;

/**
 * Class for combining multiple expressions. Every expression is represented by a {@link CompositeExpression} to allow
 * recursive evaluation.
 */
public class CompositeExpression {

    private CompositeExpression leftOperand;
    private Operator operator;
    private CompositeExpression rightOperand;

    /**
     * If the expression is just a single number, this field represents the numerical value of the number.
     */
    private Double value;

    /**
     * Constructor for a {@link CompositeExpression} when both LHS and RHS operands are themselves CompositeExpressions.
     * In this case, we need to check for the operator precedence. In the case where the {@link Operator} is {@link
     * Operator#MULTIPLY} or {@link Operator#DIVIDE}, we need to ensure that any addition or subtraction in {@link
     * #leftOperand} or {@link #rightOperand} is done after the multiplication. Since the LHS is evaluated first, then RHS
     * and only then the entire expression, this attempts to reconstruct the structure of the expressions such that
     * the multiplication or division is done as early as possible.
     */
    public CompositeExpression(CompositeExpression leftOperand, Operator operator, CompositeExpression rightOperand) {
        if (operator == Operator.ADD || operator == Operator.SUBTRACT) {
            this.leftOperand = leftOperand;
            this.operator = operator;
            this.rightOperand = rightOperand;
        } else if (leftOperand.getOperator() == Operator.ADD || leftOperand.getOperator() == Operator.SUBTRACT) {
            this.leftOperand = leftOperand.getLeftOperand();
            this.operator = leftOperand.getOperator();
            this.rightOperand = new CompositeExpression(leftOperand.getRightOperand(), operator, rightOperand);
        } else if (rightOperand.getOperator() == Operator.ADD || rightOperand.getOperator() == Operator.SUBTRACT) {
            this.leftOperand = new CompositeExpression(leftOperand, operator, rightOperand.getLeftOperand());
            this.operator = rightOperand.getOperator();
            this.rightOperand = rightOperand.getRightOperand();
        } else {
            this.leftOperand = leftOperand;
            this.operator = operator;
            this.rightOperand = rightOperand;
        }
    }

    /**
     * A {@link CompositeExpression} of the form (number operator number). See {@link ArithmeticExpression}.
     */
    public CompositeExpression(double leftOperand, Operator operator, double rightOperand) {
        this.leftOperand = new CompositeExpression(leftOperand);
        this.operator = operator;
        this.rightOperand = new CompositeExpression(rightOperand);
    }

    /**
     * A {@link CompositeExpression} of the form (number).
     */
    public CompositeExpression(Double value) {
        this.value = value;
    }

    /**
     * Recursively traverses and evaluates the {@link CompositeExpression}. The CompositeExpressions are broken down into
     * {@link ArithmeticExpression}s and then evaluated. Single numbers are immediately evaluated.
     * @return
     */
    public double evaluate() {
        // Base case - the expression is just a single number.
        if (value != null) {
            return value;
        }
        // Both LHS and RHS are numbers, thus this is an ArithmeticExpression.
        else if (leftOperand.getValue() != null && rightOperand.getValue() != null) {
            return ArithmeticExpression.createExpressionFromOperator(leftOperand.getValue(), operator, rightOperand.getValue()).evaluate();
        }
        // LHS is a number, RHS is a CompositeExpression. Create a new CompositeExpression after evaluating the RHS and
        // evaluate the new expression.
        else if (leftOperand.getValue() != null) {
            return new CompositeExpression(leftOperand.getValue(), operator, rightOperand.evaluate()).evaluate();
        }
        // RHS is a number, LHS is a CompositeExpression. Create a new CompositeExpression after evaluating the RHS and
        // evaluate the new expression.
        else if (rightOperand.getValue() != null) {
            return new CompositeExpression(leftOperand.evaluate(), operator, rightOperand.getValue()).evaluate();
        }
        // Both LHS and RHS are CompositeExpressions. Create a new CompositeExpression after evaluating the LHS and RHS.
        else {
            return new CompositeExpression(leftOperand.evaluate(), operator, rightOperand.evaluate()).evaluate();
        }
    }

    public CompositeExpression getLeftOperand() {
        return leftOperand;
    }

    public CompositeExpression getRightOperand() {
        return rightOperand;
    }

    public Operator getOperator() {
        return operator;
    }

    public Double getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "CompositeExpression{" +
                "leftOperand=    " + leftOperand +
                ", operator=   " + operator +
                ", rightOperand=   " + rightOperand +
                ", value=   " + value +
                '}';
    }
}
