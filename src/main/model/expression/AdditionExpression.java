package main.model.expression;


/**
 * Represents an addition expression (number + number).
 */
public class AdditionExpression extends ArithmeticExpression {

    public AdditionExpression(double leftOperand, double rightOperand) {
        super(leftOperand, rightOperand);
    }

    @Override
    public double evaluate() {
        return  getLeftOperand() + getRightOperand();
    }
}
