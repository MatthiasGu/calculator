package main.model.expression;

/**
 * Represents a multiplication expression (number + number).
 */
public class MultiplicationExpression extends ArithmeticExpression {
    public MultiplicationExpression(double leftOperand, double rightOperand) {
        super(leftOperand, rightOperand);
    }

    public double evaluate() {
        return  getLeftOperand() * getRightOperand();
    }
}
