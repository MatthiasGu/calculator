package main.model.expression;

/**
 * Represents a subtraction expression (number - number).
 */
public class SubtractionExpression extends ArithmeticExpression {

    public SubtractionExpression(double leftOperand, double rightOperand) {
        super(leftOperand, rightOperand);
    }

    public double evaluate() {
        return  getLeftOperand() - getRightOperand();
    }
}
