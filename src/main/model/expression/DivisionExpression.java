package main.model.expression;

/**
 * Represents a division expression (number / number).
 */
public class DivisionExpression extends ArithmeticExpression {
    public DivisionExpression(double leftOperand, double rightOperand) {
        super(leftOperand, rightOperand);
    }

    public double evaluate() {
        return getLeftOperand() / getRightOperand();
    }
}
