package main;

import main.parser.ExpressionParser;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        while (true) {
            ExpressionParser expressionParser = new ExpressionParser();
            Scanner scanner = new Scanner(System.in);
            System.out.println("Enter the expression, or 'exit' to exit the program: ");
            String userInput = scanner.nextLine();
            if (userInput.toLowerCase().equals("exit") || userInput.toLowerCase().equals("quit")) {
                break;
            }
            System.out.println(expressionParser.splitAndParseExpressions(userInput).evaluate());
        }
    }
}
