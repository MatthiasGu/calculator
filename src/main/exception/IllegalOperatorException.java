package main.exception;

/**
 * Indicates that the value {@link #operator} is an illegal representation of an arithmetic operator with regards to
 * {@link main.parser.OperatorParser}.
 */
public class IllegalOperatorException extends Exception {

    private String operator;

    public IllegalOperatorException() {
        super();
    }

    public IllegalOperatorException(String operator) {
        super();
        this.operator = operator;
    }

    public String getMessage() {
        return "Operator " + operator + " is not a correct verbal representation of an arithmetic operator that is" +
                "accepted by this calculator!";
    }
}
