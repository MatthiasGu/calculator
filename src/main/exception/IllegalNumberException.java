package main.exception;

/**
 * Indicates that the value {@link #number} is an illegal representation of a number with regards to
 * {@link main.parser.NumberParser}.
 */
public class IllegalNumberException extends Exception {

    private String number;

    public IllegalNumberException() {
        super();
    }

    public IllegalNumberException(String number) {
        super();
        this.number = number;
    }

    public String getMessage() {
        return "Number " + number + " is not a number or is not accepted by this calculator! Please enter a number between " +
                "zero and ten (inclusive) in words.";
    }
}
