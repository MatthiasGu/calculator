package parser;

import main.parser.ExpressionParser;
import org.junit.*;

/**
 * Created by mgudenas on 22/09/2017.
 */
public class ExpressionParserTest {
    private String testString;
    private ExpressionParser expressionParser;

    @Before
    public void setUp() {
        expressionParser = new ExpressionParser();
    }

    @Test
    public void testExpressionParserParsesSingleNumberAsAValue() {
        testString = "five";
        Assert.assertEquals(expressionParser.splitAndParseExpressions(testString).getValue(), 5, 0.0);

    }

    @Test
    public void testExpressionParserParsesArithmeticExpressionsCorrectly() {
        testString = "two plus two";
        Assert.assertEquals(expressionParser.splitAndParseExpressions(testString).evaluate(), 4, 0.0);
    }

    @Test
    public void testExpressionParserParsesCompositeExpressionCorrectly() {
        testString = "five plus six plus five times two divided-by ten plus one minus zero";
        Assert.assertEquals(expressionParser.splitAndParseExpressions(testString).evaluate(), 13, 0.0);
    }
}