package parser;

import main.exception.IllegalNumberException;
import main.parser.NumberParser;
import org.junit.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * Created by mgudenas on 22/09/2017.
 */
public class NumberParserTest {
    private String testString;
    private NumberParser numberParser;

    @Before
    public void setUp() {
        numberParser = new NumberParser();
    }

    @Test
    public void testParseNumberWorksForZero() {
        try {
            testString = "zero";
            assertEquals(0 , numberParser.parseNumber(testString), 0.0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testParseNumberIsCaseInsensitive() {
        try {
            testString = "SeVeN";
            assertEquals(7, numberParser.parseNumber(testString), 0.0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testParseNumberThrowsExceptionForIllegalNumber() {
        testString = "WhatEver";
        try {
            numberParser.parseNumber(testString);
            fail();
        } catch (Exception e) {
            assertEquals(e.getClass(), IllegalNumberException.class);
        }
    }
}
