package parser;
import main.exception.IllegalOperatorException;
import main.model.operator.Operator;
import main.parser.OperatorParser;
import org.junit.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * Created by mgudenas on 22/09/2017.
 */
public class OperatorParserTest {

    private String testString;
    private  OperatorParser operatorParser;

    @Before
    public void setUp() {
        operatorParser = new OperatorParser();
    }

    @Test
    public void testParseOperatorWorksForAdd() {
        try {
            testString = "add";
            assertEquals(Operator.ADD, operatorParser.parseOperator(testString));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testParseOperatorIsCaseInsensitive() {
        try {
            testString = "SuBtRaCt";
            assertEquals(Operator.SUBTRACT, operatorParser.parseOperator(testString));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testParseOperatorThrowsExceptionForIllegalOperator() {
            testString = "WhatEver";
        try {
            operatorParser.parseOperator(testString);
            fail();
        } catch (Exception e) {
            assertEquals(e.getClass(), IllegalOperatorException.class);
        }
    }

}
