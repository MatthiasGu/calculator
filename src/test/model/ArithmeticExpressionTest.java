package model;

import main.model.expression.ArithmeticExpression;
import main.model.operator.Operator;
import org.junit.*;

import static org.junit.Assert.assertEquals;

/**
 * Created by mgudenas on 22/09/2017.
 */
public class ArithmeticExpressionTest {

    private final double LEFT = 10;
    private final double RIGHT = 5;
    private ArithmeticExpression arithmeticExpression;

    @Test
    public void testAdditionExpressionEvaluatesCorrectly() {
        arithmeticExpression = ArithmeticExpression.createExpressionFromOperator(LEFT, Operator.ADD, RIGHT);
        assertEquals(15, arithmeticExpression.evaluate(), 0.0);
    }

    @Test
    public void testSubtractionExpressionEvaluatesCorrectly() {
        arithmeticExpression = ArithmeticExpression.createExpressionFromOperator(LEFT, Operator.SUBTRACT, RIGHT);
        assertEquals(5, arithmeticExpression.evaluate(), 0.0);
    }

    @Test
    public void testMultiplicationExpressionEvaluatesCorrectly() {
        arithmeticExpression = ArithmeticExpression.createExpressionFromOperator(LEFT, Operator.MULTIPLY, RIGHT);
        assertEquals(50, arithmeticExpression.evaluate(), 0.0);
    }

    @Test
    public void testDivisionExpressionEvaluatesCorrectly() {
        arithmeticExpression = ArithmeticExpression.createExpressionFromOperator(LEFT, Operator.DIVIDE, RIGHT);
        assertEquals(2, arithmeticExpression.evaluate(), 0.0);
    }
}
