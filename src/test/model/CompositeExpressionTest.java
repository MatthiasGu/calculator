package model;
import main.model.expression.CompositeExpression;
import main.model.operator.Operator;
import org.junit.*;

import static org.junit.Assert.assertEquals;

/**
 * Created by mgudenas on 22/09/2017.
 */
public class CompositeExpressionTest {

    private final double LEFT = 10;
    private final double RIGHT = 5;
    private CompositeExpression compositeExpression;

    @Test
    public void testSingleNumberEvaluatesCorrectly() {
        compositeExpression = new CompositeExpression(LEFT);
        assertEquals(LEFT, compositeExpression.evaluate(), 0.0);
    }

    @Test
    public void testAdditionExpressionEvaluatesCorrectly() {
        compositeExpression = new CompositeExpression(LEFT, Operator.ADD, RIGHT);
        assertEquals(15, compositeExpression.evaluate(), 0.0);
    }

    @Test
    public void testCompositeExpressionEvaluatesCorrectly() {
        CompositeExpression leftOperand = new CompositeExpression(LEFT, Operator.ADD, RIGHT);
        CompositeExpression rightOperand = new CompositeExpression(LEFT, Operator.SUBTRACT, RIGHT);
        compositeExpression = new CompositeExpression(leftOperand, Operator.MULTIPLY, rightOperand);
        assertEquals(10+5*10-5, compositeExpression.evaluate(), 0.0);
    }

    @Test
    public void testCompositeExpressionEvaluatesCorrectlyWhenLHSIsANumber() {
        CompositeExpression leftOperand = new CompositeExpression(LEFT);
        CompositeExpression rightOperand = new CompositeExpression(LEFT, Operator.SUBTRACT, RIGHT);
        compositeExpression = new CompositeExpression(leftOperand, Operator.MULTIPLY, rightOperand);
        assertEquals(10*10-5, compositeExpression.evaluate(), 0.0);
    }

    @Test
    public void testCompositeExpressionEvaluatesCorrectlyWhenRHSIsANumber() {
        CompositeExpression leftOperand = new CompositeExpression(LEFT, Operator.ADD, RIGHT);
        CompositeExpression rightOperand = new CompositeExpression(RIGHT);
        compositeExpression = new CompositeExpression(leftOperand, Operator.MULTIPLY, rightOperand);
        assertEquals(10+5*5, compositeExpression.evaluate(), 0.0);
    }

    @Test
    public void testCompositeExpressionEvaluatesCorrectlyWhenLHSAndRHSAreCompositeExpressions() {
        CompositeExpression leftOperand = new CompositeExpression(
                new CompositeExpression(LEFT, Operator.SUBTRACT, RIGHT),
                Operator.MULTIPLY,
                new CompositeExpression(LEFT));
        CompositeExpression rightOperand = new CompositeExpression(
                new CompositeExpression(LEFT, Operator.ADD, RIGHT),
                Operator.DIVIDE,
                new CompositeExpression(RIGHT));
        compositeExpression = new CompositeExpression(leftOperand, Operator.MULTIPLY, rightOperand);
        assertEquals(10-(5*10*10+5/5), compositeExpression.evaluate(), 0.0);
    }
}
